# Kubernetes Cluster
### Auf Master ausführen:
`sudo kubeadm init --pod-network-cidr=192.168.0.0/16`

`mkdir -p $HOME/.kube`

`sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config`

`sudo chown $(id -u):$(id -g) $HOME/.kube/config`

`kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml`

`kubeadm token create --print-join-command`




### auf Worken ausgegebeen Befehl ausführen
zum zurücksetzen 
`kubeadm reset`

`rm -rf /etc/cni/net.d`

## Installationsprozess
### Docker
`sudo apt install apt-transport-https ca-certificates curl software-properties-common`

`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`

`sudo apt update`

`sudo apt install docker-ce`
### Kubernetes

`sudo apt-get update && sudo apt-get install -y apt-transport-https curl`

`curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -`

`cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list`

`deb https://apt.kubernetes.io/ kubernetes-xenial main`

`EOF`

`sudo apt-get update`

`sudo apt-get install -y kubelet kubeadm kubectl`

`sudo apt-mark hold kubelet kubeadm kubectl`

#### Master
(init)

### Worker:
(Join Command)

zum Prüfen:  `kubectl get nodes`


## mongo DB (test)-installation
auf anderem Server

`sudo apt-get install gnupg`

`wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -`

`echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list`

`sudo apt-get update`

`sudo apt-get install -y mongodb-org `

`sudo vi /etc/mongod.conf  `

`0.0.0.0` bei der bind IPmongo 

`sudo systemctl start mongod`

27017 Port Freigabe!

Wichtig: Server komplett neustarten / Reboot

## Service laufen lassen
`kubectl apply -f piep.yaml`

`kubectl expose deployment piep-deployment --type=NodePort --name=piep`


# Website Testen
### Benutzer
http://54.159.122.154:31245/users/

### Feed
http://54.159.122.154:31245/feedme/Johannes 

`http://172.23.1.3:31233/`

### Daten füllen
`curl  -H "Content-Type:application/json" -X POST 54.159.122.154:31245/piep  --data '{"username":"Johannes","msg":"test2"}'`

# Metric Server
auf Master 

`kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml`

`kubectl edit deployment metrics-server -n kube-system`
 
hinzufügen nach image, releativ weit unten
command:
        - /metrics-server
        - --kubelet-insecure-tls

## Autoscale


`kubectl autoscale deploy piep-deployment --min 1 --max 80 --cpu-percent 20`

oder lang per yaml_:

`git clone https://github.com/justmeandopensource/kubernetes`

`cp /kubernetes/yamls/10-hpa.yaml hpa.yaml`

`vi hpa.yaml`
Ändern
	name = pipe

`kubectl create -f hpa.yaml`

Danach

Resources aus yaml ziehen ,oder alternativ:
`kubectl edit deploy piep-deployment` und die  Ressourcen:{} überschreiben:
resources:
   limits:
     cpu: 100m
   requests:
     cpu: 100m

klammer {} muss weg, immer 2 leerzeichen zur einrückung


### Checken, ob's klappt:
`kubectl -n kube-system get pods`

`kubectl -n kube-system logs metrics-server-7c67f94f7-q2snx`

`kubectl top nodes`

### Um zu Löschen :
`kubectl -n kube-system delete pod metrics-server-7c67f94f7-cdd8h`

`replicaset.apps/piep-deployment-6c4545c5f4`

`kubectl delete hpa piep-deployment`

`kubectl delete deployment piep-deployment`

## Hilfe & infos

sehen was an ist, automatisch aktualliseren

`watch kubectl get all` HPA funktioniert erst wenn beide % Zahlen anzeigt, undefined verschwindet nach ca. 1 Minute nach dem Start

Infos zum Deployment

`kubectl describe deploy piep-deployment`

`kubectl top pod piep-deployment-6d95fb9674-nf6lh`

`kubectl describe nodes``


Infos zum Horizontal Pod Autoscaler

`kubectl describe hpa piep-deployment`

`kubectl delete hpa  --all`

## Test
evtl. installieren  `sudo apt-get install siege`

`siege -q -c 10 -t 2m http://54.198.43.30:31245/feedme/Johannes`

## Befehle zum manuellen Test:

`http://g1-jp-master.wi.fh-flensburg.de:31233/feedme/Johannes`

`g1-jp-master.wi.fh-flensburg.de:31233/follow?me=Leon&follow=Marie`

`g1-jp-master.wi.fh-flensburg.de:31233/users`

`curl  -H "Content-Type:application/json" -X POST g1-jp-master.wi.fh-flensburg.de:31233/piep  --data '{"username":"Jens","msg":"Was geht ab"}'`

`while True; do curl  -H "Content-Type:application/json" -X POST g1-master.wi.fh-flensburg.de:30938/piep  --data '{"username":"Jens","msg":"Was geht ab"}';done`
